# Dotfiles

To install any of my configs please use stow.

Learn more [here](https://www.gnu.org/software/stow/) or you can type:
```
% man stow
```

## Examples:

Stow everything or stow a selection of packages:
```
% stow *
% stow nvim alacritty
```

If you cloned this repo outside your home folder, the target *-t* option can be used:
```
% stow -t ~/ polybar rofi bspwm sxhkd
```

And to remove any stow:
```
% stow -D nvim
% stow -t ~/ -D polybar
```
