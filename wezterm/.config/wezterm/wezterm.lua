local wezterm = require 'wezterm'

return {
  font = wezterm.font("TerminessTTF Nerd Font Mono"),
  -- underline_position = "800%",
  -- underline_thickness = "1.5pt",
  font_size = 12.0,

  color_scheme = "Catppuccin Mocha",
}

