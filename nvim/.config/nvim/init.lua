local opt = vim.opt
local cmd = vim.cmd
local fn = vim.fn
local g = vim.g
local api = vim.api

cmd [[packadd packer.nvim]]
require('packer').startup(function(use)
  use 'wbthomason/packer.nvim'

  -- functionalities
  use 'nvim-lua/plenary.nvim'
  use {
    'nvim-telescope/telescope.nvim',
    requires = {
      { 'nvim-telescope/telescope-fzy-native.nvim' },
      { 'nvim-treesitter/nvim-treesitter' }
    }
  }
  use 'mbbill/undotree'
  use 'tpope/vim-fugitive'
  use 'tpope/vim-eunuch'
  use 'airblade/vim-gitgutter'
  use 'justinmk/vim-sneak'
  use 'unblevable/quick-scope'
  use 'tpope/vim-commentary'
  use 'ThePrimeagen/harpoon'
  use 'kevinhwang91/rnvimr'
  use 'psliwka/vim-smoothie'
  use {
    'kevinhwang91/nvim-ufo',
    requires = 'kevinhwang91/promise-async'
  }

  -- languages stuff and lsp
  use 'sheerun/vim-polyglot'
  use 'neovim/nvim-lspconfig'
  use 'williamboman/nvim-lsp-installer'
  use 'nvim-treesitter/nvim-treesitter'
  use 'JoosepAlviste/nvim-ts-context-commentstring'
  use {
    'hrsh7th/nvim-cmp',
    requires = {
      { 'hrsh7th/cmp-nvim-lsp' },
      { 'hrsh7th/cmp-buffer' },
      { 'hrsh7th/cmp-path' },
      { 'hrsh7th/cmp-cmdline' },
      { 'L3MON4D3/LuaSnip' },
    }
  }
  use 'akinsho/flutter-tools.nvim'

  -- fluffy
  use 'EdenEast/nightfox.nvim'
  use 'hoob3rt/lualine.nvim'
  use 'folke/zen-mode.nvim'
  use 'szw/vim-maximizer'
end)

-- GENERAL OPTIONS
opt.exrc = true

opt.mouse = 'a'

opt.splitright = true
opt.splitbelow = true

opt.expandtab = true
opt.tabstop = 2

opt.shiftwidth = 2
cmd('filetype plugin indent on')

opt.number = true

opt.hls = false
opt.incsearch = true

opt.hidden = true

opt.cmdheight = 1

opt.updatetime = 500
opt.backup = false
opt.swapfile = false
opt.undofile = true
opt.undolevels = 9999

opt.belloff = 'all'

g.python3_host_prog = '~/.asdf/shims/python'

-- Leaving this here to enable when the new nvim release comes out
-- opt.laststatus = 3

g.netrw_banner = 0
g.netrw_liststyle = 3
g.netrw_winsize = 25

-- MOVEMENT
-- quickscope
g.qs_highlight_on_keys = { 'f', 'F', 't', 'T' }
cmd([[
  augroup qs_colors
    autocmd!
    autocmd ColorScheme * highlight QuickScopePrimary guifg='#458588' gui=underline ctermfg=155 cterm=underline
    autocmd ColorScheme * highlight QuickScopeSecondary guifg='#b16286' gui=underline ctermfg=81 cterm=underline
  augroup END
]])
-- sneak
g['sneak#label'] = 1
g['sneak#use_ic_scs'] = 1
g['sneak#s_next'] = 1

-- rnvimr
g.rnvimr_enable_ex = 1
g.rnvimr_enable_picker = 1

-- FLUFFY
-- opt.background = 'dark'
-- g.gruvbox_contrast_dark = 'hard'
-- cmd('colorscheme gruvbox')

local nightfox = require('nightfox')
nightfox.setup({
  options = {
    styles = {
      comments = "italic", -- change style of comments to be italic
      keywords = "bold", -- change style of keywords to be bold
      functions = "italic,bold" -- styles can be a comma separated list
    },
  }
})
cmd('colorscheme dayfox')

opt.scrolloff = 8
opt.signcolumn = 'yes'
opt.termguicolors = true
opt.wrap = false
require('lualine').setup({
  options = {
    icons_enabled = false,
    theme = 'dayfox',
    component_separators = { '', '' },
    section_separators = { '', '' },
    disabled_filetypes = {}
  },
  sections = {
    lualine_a = { 'mode' },
    lualine_b = { 'branch' },
    lualine_c = { 'filename' },
    lualine_x = { 'encoding', 'fileformat', 'filetype' },
    lualine_y = { 'progress' },
    lualine_z = { 'location' }
  },
  inactive_sections = {
    lualine_a = {},
    lualine_b = {},
    lualine_c = { 'filename' },
    lualine_x = { 'location' },
    lualine_y = {},
    lualine_z = {}
  },
  tabline = {},
  extensions = {}
})
opt.showmode = false

local function map(mode, lhs, rhs, opts)
  local options = { noremap = true }
  if opts then options = vim.tbl_extend('force', options, opts) end
  api.nvim_set_keymap(mode, lhs, rhs, options)
end

require('telescope').setup {
  defaults = {
    file_sorter = require('telescope.sorters').get_fzy_sorter,
  },
  pickers = {},
  extensions = {
    fzy_native = {
      override_generic_sorter = false,
      override_file_sorter = true,
    }
  }
}

-- TELESCOPE
require('telescope').load_extension('fzy_native')

_G.find_files_project = function()
  local utils = require('telescope.utils')
  local builtin = require('telescope.builtin')

  local _, res, _ = utils.get_os_command_output({ 'git', 'rev-parse', '--is-inside-work-tree' })

  if res == 0 then
    builtin.git_files()
  else
    builtin.find_files({ hidden = true })
  end
end

_G.cd_into_dir_project = function()
  local utils = require('telescope.utils')
  local builtin = require('telescope.builtin')

  cmd('cd %:p:h')
  local stdout, _, _ = utils.get_os_command_output({ 'git', 'rev-parse', '--git-dir' })

  if next(stdout) ~= nil and stdout[1] ~= '.git' then
    local cleaned_path = string.gsub(stdout[1], '/.git', '')
    cmd('cd ' .. cleaned_path)
  end
end

-- TREESITTER
require 'nvim-treesitter.configs'.setup {
  -- One of "all", "maintained" (parsers with maintainers), or a list of languages
  ensure_installed = "all",

  -- Install languages synchronously (only applied to `ensure_installed`)
  sync_install = false,

  -- List of parsers to ignore installing
  -- ignore_install = { "javascript" },

  highlight = {
    enable = true,
    additional_vim_regex_highlighting = false,
    -- disable = { "c", "rust" },
  },
  context_commentstring = {
    enable = true,
  }
}

-- vim.opt.foldmethod="expr"
-- vim.opt.foldexpr="nvim_treesitter#foldexpr()"

-- LSP
local has_words_before = function()
  local line, col = unpack(vim.api.nvim_win_get_cursor(0))
  return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
end

local nvim_lsp_installer = require("nvim-lsp-installer")
local cmp = require('cmp')
local luasnip = require("luasnip")
local nvim_lsp = require('lspconfig')

nvim_lsp_installer.setup {}

cmp.setup({
  snippet = {
    expand = function(args)
      require('luasnip').lsp_expand(args.body)
    end,
  },
  mapping = {
    ['<C-j>'] = cmp.mapping.scroll_docs(-4),
    ['<C-k>'] = cmp.mapping.scroll_docs(4),
    ['<C-Space>'] = cmp.mapping.complete(),
    ['<C-e>'] = cmp.mapping.close(),
    ['<CR>'] = cmp.mapping.confirm({ select = true }),

    ["<Tab>"] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_next_item()
      elseif luasnip.expand_or_jumpable() then
        luasnip.expand_or_jump()
      elseif has_words_before() then
        cmp.complete()
      else
        fallback()
      end
    end, { "i", "s" }),
    ["<S-Tab>"] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_prev_item()
      elseif luasnip.jumpable(-1) then
        luasnip.jump(-1)
      else
        fallback()
      end
    end, { "i", "s" }),
  },
  sources = cmp.config.sources({
    { name = 'nvim_lsp' },
    { name = 'luasnip' },
  }, {
    { name = 'buffer' },
  })
})

-- on_attach function to only map the following keys
-- after the language server attaches to the current buffer
local on_attach = function(client, bufnr)
  if client.name == 'tsserver' then
    if client.config.flags then
      client.config.flags.allow_incremental_sync = true
    end
    client.resolved_capabilities.document_formatting = false
  elseif client.name == 'efm' then
    client.resolved_capabilities.document_formatting = true
    client.resolved_capabilities.goto_definition = false
  end

  local function buf_set_keymap(...)
    api.nvim_buf_set_keymap(bufnr, ...)
  end

  local function buf_set_option(...)
    api.nvim_buf_set_option(bufnr, ...)
  end

  -- Enable completion triggered by <c-x><c-o>
  buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')

  -- Mappings.
  local opts = { noremap = true, silent = true }

  buf_set_keymap('n', 'gD', ':lua vim.lsp.buf.declaration()<CR>', opts)
  buf_set_keymap('n', 'gd', ':lua vim.lsp.buf.definition()<CR>', opts)
  buf_set_keymap('n', 'gi', ':lua vim.lsp.buf.implementation()<CR>', opts)
  buf_set_keymap('n', 'K', ':lua vim.lsp.buf.hover()<CR>', opts)
  buf_set_keymap('n', '<space>rn', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
  buf_set_keymap('n', '[d', '<cmd>lua vim.diagnostic.goto_prev()<CR>', opts)
  buf_set_keymap('n', ']d', '<cmd>lua vim.diagnostic.goto_next()<CR>', opts)

  -- used to highlight when the cursor doesn't move for period of time
  cmd([[autocmd ColorScheme * :lua require('vim.lsp.diagnostic')._define_default_signs_and_highlights()]])
  cmd([[autocmd CursorHold  <buffer> lua vim.lsp.buf.document_highlight()]])
  cmd([[autocmd CursorHoldI <buffer> lua vim.lsp.buf.document_highlight()]])
  cmd([[autocmd CursorMoved <buffer> lua vim.lsp.buf.clear_references()]])

  -- format on save
  -- cmd([[autocmd BufWritePre <buffer> lua vim.lsp.buf.formatting_sync()]])
  -- format keymap
  buf_set_keymap('n', '<F9>', ':lua vim.lsp.buf.formatting_sync()<CR>', opts)

  -- buf_set_keymap('n', '<C-k>', '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
  -- buf_set_keymap('n', '<space>wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', opts)
  -- buf_set_keymap('n', '<space>wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', opts)
  -- buf_set_keymap('n', '<space>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', opts)
  -- buf_set_keymap('n', '<space>D', '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
  -- buf_set_keymap('n', '<space>ca', '<cmd>lua vim.lsp.buf.code_action()<CR>', opts)
  -- buf_set_keymap('n', 'gr', '<cmd>lua vim.lsp.buf.references()<CR>', opts)
  -- buf_set_keymap('n', '<space>e', '<cmd>lua vim.lsp.diagnostic.show_line_diagnostics()<CR>', opts)
  -- buf_set_keymap('n', '<space>q', '<cmd>lua vim.lsp.diagnostic.set_loclist()<CR>', opts)
  -- buf_set_keymap('n', '<space>f', '<cmd>lua vim.lsp.buf.formatting()<CR>', opts)
end

local capabilities = require('cmp_nvim_lsp').update_capabilities(vim.lsp.protocol.make_client_capabilities())
capabilities.textDocument.foldingRange = {
  dynamicRegistration = false,
  lineFoldingOnly = true
}

-- basic tsserver config
nvim_lsp.tsserver.setup {
  on_attach = on_attach,
  capabilities = capabilities,
  flags = {
    debounce_text_changes = 150,
  },
}

-- basic haskell ls config
nvim_lsp.rust_analyzer.setup {
  on_attach = on_attach,
  capabilities = capabilities,
}

-- basic haskell ls config
nvim_lsp.hls.setup {
  on_attach = on_attach,
  capabilities = capabilities,
}

-- basic golang config
nvim_lsp.gopls.setup {
  on_attach = on_attach,
  capabilities = capabilities,
  settings = {
    gopls = {
      analyses = {
        unusedparams = true,
      },
      staticcheck = true,
    },
  },
}

nvim_lsp.sumneko_lua.setup {
  on_attach = on_attach,
  capabilities = capabilities,
  settings = {
    Lua = {
      runtime = {
        -- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
        version = 'LuaJIT',
      },
      diagnostics = {
        -- Get the language server to recognize the `vim` global
        globals = { 'vim' },
      },
      workspace = {
        -- Make the server aware of Neovim runtime files
        library = vim.api.nvim_get_runtime_file("", true),
      },
      -- Do not send telemetry data containing a randomized but unique identifier
      telemetry = {
        enable = false,
      },
    },
  },
}

-- flutter tools setup
require("flutter-tools").setup {
  flutter_lookup_cmd = "asdf where flutter",
  lsp = {
    on_attach = on_attach,
    capabilities = capabilities,
  }
}

-- eslint config
-- thanks to https://phelipetls.github.io/posts/configuring-eslint-to-work-with-neovim-lsp/
local function eslint_config_exists()
  local eslintrc = vim.fn.glob(".eslintrc*", 0, 1)

  if not vim.tbl_isempty(eslintrc) then
    return true
  end

  return false
end

local eslint = {
  lintCommand = "eslint_d -f unix --stdin --stdin-filename ${INPUT}",
  lintStdin = true,
  lintFormats = { "%f:%l:%c: %m" },
  lintIgnoreExitCode = true,
  formatCommand = "eslint_d --fix-to-stdout --stdin --stdin-filename=${INPUT}",
  formatStdin = true
}

nvim_lsp.efm.setup {
  on_attach = on_attach,
  root_dir = function()
    if not eslint_config_exists() then
      return nil
    end
    return vim.fn.getcwd()
  end,
  settings = {
    languages = {
      javascript = { eslint },
      javascriptreact = { eslint },
      ["javascript.jsx"] = { eslint },
      typescript = { eslint },
      ["typescript.tsx"] = { eslint },
      typescriptreact = { eslint }
    }
  },
  filetypes = {
    "javascript",
    "javascriptreact",
    "javascript.jsx",
    "typescript",
    "typescript.tsx",
    "typescriptreact"
  },
  flags = {
    debounce_text_changes = 150,
  },
  capabilities = require('cmp_nvim_lsp').update_capabilities(vim.lsp.protocol.make_client_capabilities()),
}

-- FOLD
vim.wo.foldcolumn = '1'
vim.wo.foldlevel = 99
vim.wo.foldenable = true

-- GLOBAL KEYMAPS
g.mapleader = ' '

-- split to left
map('n', '<leader>sl', ':vs<CR>')
-- quit
map('n', '<leader>wd', ':q<CR>')
-- file save
map('n', '<leader>fs', ':w<CR>')
-- navigate left
map('n', '<leader>wl', '<C-w>l')
-- navidate right
map('n', '<leader>wh', '<C-w>h')
-- navigate up
map('n', '<leader>wk', '<C-w>k')
-- navidate down
map('n', '<leader>wj', '<C-w>j')
-- clear everything
map('n', '<leader>wo', '<C-w>o')
-- change to current directory of the file
map('n', '<leader>cd', ':lua cd_into_dir_project()<CR>:pwd<CR>')
-- maximize
map('n', '<leader>wm', ':MaximizerToggle!<CR>')
-- zen mode
map('n', '<leader>mm', ':ZenMode<CR>')

-- telescop
-- find file
map('n', '<leader><leader>', ':lua find_files_project()<CR>')
-- search buffers
map('n', '<leader>ss', [[:lua require('telescope.builtin').live_grep()<CR>]])
-- search history
map('n', '<leader>sh', [[:lua require('telescope.builtin').search_history()<CR>]])
-- search buffersunder cursor
map('n', '<leader>*', [[:lua require('telescope.builtin').grep_string()<CR>]])
-- open buffers
map('n', '<leader>bb', [[:lua require('telescope.builtin').buffers()<CR>]])
-- open command history
map('n', '<F1>', [[:lua require('telescope.builtin').command_history()<CR>]])
-- git branches
map('n', '<leader>gb', [[:lua require('telescope.builtin').git_branches()<CR>]])
-- git commits
map('n', '<leader>gc', [[:lua require('telescope.builtin').git_commits()<CR>]])
-- git status
map('n', '<leader>gs', [[:lua require('telescope.builtin').git_status()<CR>]])

-- toggle between buffer
map('n', '<leader>bl', '<C-^>')
-- undo tree
map('n', '<F5>', ':UndotreeToggle<CR>')
-- git
map('n', '<leader>gg', ':Git<CR>')
-- window tree
-- map('n', '<leader>ff', ':Lex<CR>')
map('n', '<leader>ff', ':RnvimrToggle<CR>')
-- stop sneak from hijacking , and ;
map('', 'gs', '<Plug>Sneak_;')
map('', 'gS', '<Plug>Sneak_,')

-- harpoon
map('n', '<leader>1', [[:lua require('harpoon.ui').nav_file(1)<CR>]]);
map('n', '<leader>2', [[:lua require('harpoon.ui').nav_file(2)<CR>]]);
map('n', '<leader>3', [[:lua require('harpoon.ui').nav_file(3)<CR>]]);
map('n', '<leader>4', [[:lua require('harpoon.ui').nav_file(4)<CR>]]);
map('n', '<leader>5', [[:lua require('harpoon.ui').nav_file(5)<CR>]]);
map('n', '<leader>ha', [[:lua require('harpoon.mark').add_file()<CR>]]);
map('n', '<leader>hh', [[:lua require('harpoon.ui').toggle_quick_menu()<CR>]]);
