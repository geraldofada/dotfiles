;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Geraldo Fada"
      user-mail-address "geraldofada.neto@gmail.com")


;; Fluffy
(setq doom-font (font-spec :family "JetBrains Mono" :size 13 ))
(setq doom-theme 'doom-tomorrow-day)
(setq display-line-numbers-type 'relative)
(setq fancy-splash-image "~/.config/doom/black-hole.png") ;; change splash art
