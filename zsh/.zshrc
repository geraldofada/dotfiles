# ----- Oh my zsh
export ZSH="$HOME/.oh-my-zsh"

plugins=(
    git
    tmux
    vi-mode
    # catppuccin_mocha-zsh-syntax-highlighting
    zsh-syntax-highlighting
)

VI_MODE_SET_CURSOR=true

source $ZSH/oh-my-zsh.sh

# ----- Personal

# fix tmux colors
TERM=xterm-256color

# Aliases
alias ls="exa"
alias rm="rm -I"
alias ll="exa -la"
alias lla="exa -l"
alias fls="nautilus ."
alias scrub="sudo btrfs scrub start /"
alias balance="sudo btrfs balance start -musage=50 -dusage=50 /"
alias g="git"
alias gi="gitui -t mocha.ron"
alias v="nvim"
alias t="zellij"
alias td="zellij --layout dev"
alias fp=". find-project"
alias rr="ranger"
alias ht="history | sort -rn | fzf"

# ENV VARS
export PATH="$PATH:$HOME/.local/bin"
# cargo bin
export PATH="$PATH:$HOME/.cargo/bin"
# ghcup env
export PATH="$PATH:$HOME/.ghcup/bin:$HOME/.cabal/bin"
# androidsdk and flutter root
export PATH="$PATH:$HOME/.androidsdk/cmdline-tools/latest/bin"
# bob nvim version manager
export PATH="$PATH:$HOME/.local/share/bob/nvim-bin"
# .net
export DOTNET_ROOT=$HOME/.dotnet
export PATH=$PATH:$HOME/.dotnet:$HOME/.dotnet/tools
# rebar3
export PATH=$PATH:/home/geraldofada/.cache/rebar3/bin
# doom emacs
export PATH=$PATH:/home/geraldofada/.config/emacs/bin

export EDITOR="nvim"

export FZF_DEFAULT_OPTS=" \
--color=bg+:#313244,bg:#1e1e2e,spinner:#f5e0dc,hl:#f38ba8 \
--color=fg:#cdd6f4,header:#f38ba8,info:#cba6f7,pointer:#f5e0dc \
--color=marker:#f5e0dc,fg+:#cdd6f4,prompt:#cba6f7,hl+:#f38ba8"

eval "$(starship init zsh)"
eval "$(mise activate zsh)"

# ghcup-env
[ -f "/home/geraldofada/.ghcup/env" ] && source "/home/geraldofada/.ghcup/env" 

# ocaml env
[[ ! -r "/home/geraldofada/.opam/opam-init/init.zsh" ]] || source "/home/geraldofada/.opam/opam-init/init.zsh"  > /dev/null 2> /dev/null
